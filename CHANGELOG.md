# `Simi-cli` Change Log

---
## v0.1.8
### Added
* simi build --stable

## v0.1.7
Released 2019-03-22.

### Added

* File watcher for dev server (thanks [Daniel Schindler](https://gitlab.com/tw1t611))

### Changed

* Upgrade rust to edition 2018
* `wasm-bindgen --web` instead of `--browser`. This remove the need of `wasm2es6js`, yay!
* Update dependencies

## v0.1.6

### Added
* `simi serve only`

### Fixed
* Get version number from `CARGO_PKG_VERSION`

## v0.1.5
Released 2018-10-09.

* Moved `simi-cli` into its own repo (to avoid running ci test for simi framework if we just change cli source code)

### Fixed

* Properly error on invalid arguments in `.simi.toml`

## v0.1.4
Released 2018-10-08.

### Fixed

* external_cmds::copy for std::fs::copy

## v0.1.3
Released 2018-10-08.

### Added

* Support for `output_path` and `wasm_serve_path` in `.simi.toml`

## 0.1.2
Released 2018-10-05.

### Changed

* Use --browser instead of --no-modules

## v0.1.1
Released 2018-10-04.

### Added
Copy files from `static` to `output`

## v0.1.0

Released 2018-10-01.

### Added

* Simi-cli
    * Support scss, thank to [`rsass`](https://github.com/kaj/rsass)

### Changed

* Simi-cli
    * `simi.toml` to `.simi.toml`


## Template
(Do not add to this section, please add change to the topmost section)

Released YYYY-MM-DD.

### Added

* TODO 

### Changed

* TODO 

### Deprecated

* TODO 

### Removed

* TODO 

### Fixed

* TODO 

### Security

* TODO 
