# simi-cli

Provide `simi`, a cli tool for creating and building [`simi` frontend web app](https://crates.io/crates/simi).

`simi` command is built on a Fedora system. I am not sure if it works on other systems or not.

`simi build` build the app.
`simi serve` build and serve the app. The file watcher watches for changes in `src/` and `static/`. 
`simi test` run tests in headless browser

## Output

`simi-cli` will output at least 4 files: `<app_name>.js`, `<app_name>_bg.js`, `<app_name>_bg.wasm` and `index.html`. Where `<app_name>` is the `package.name` in your `Cargo.toml` (replace any `-` by `_`).

## `.simi.toml`

You can specify some argument to `simi-cli` via this file.

**`output_path = "..."`**

This is the location where `simi-cli` will output built-files and copy `crate-root/static/*` to.

Default value for `output_path` is `crate-root/simi-site`, where `crate-root` is the folder where your `Cargo.toml` is.

**`index = "..."`**

This specifies the path of `index.html` file to be copied to `output_path`. Default value is `crate-root/static/index.html`. If it is not specified and the default does not exist then `simi-cli` will generate a default `index.html`.

**`scss = "..."`**

Specify the `.scss` file for `simi-cli` to compile into `.css` into `output_path`.

Default value is `crate-root/static/style.scss`.

If the `index.html` is auto generated one, `simi-cli` will add a `<link>` it for the compiled `.css`.

**`wasm_serve_path = "..."`**

If you want to serve your wasm-app files under a sub route, for example: `example.com/static-files/wasm-app/*`, then you must specify this argument like:
```
wasm_serve_path = "static-files/wasm-app"
```
Currently, the `.wasm` file is being loaded by `fetch`. If we do not supply the wasm_serve_path, `fetch` always try to load the file at root.

## Your own `index.html`

If you want to provide your own `index.html`, you should place it at `crate-root/static/index.html`. `simi-cli` will just copy this file to `output_path` without any modification. Therefore, you must add:
```
<script src="<serve_path/app_name>_bg.js" type="module"></script>
                                          ^^^^^^^^^^^^^ <= Important!
```
in your `index.html` (see information about `<app_name>` in Section `Output` above).

If you also have a `crate-root/static/style.scss`, `simi-cli` still compile and output it to `output_path`, but you also must add:
```
<link rel='stylesheet' href='serve_path/style.css'>
```
to your `index.html`.

## Contribution

Please record your changes in `CHANGELOG.md`

#### simi-cli TODO:
Currently, there is no tests for `simi-cli`. But if you want to contribute this crate, you are required to add tests (at least for your own changes).
- [x] `serve` Support some kind of auto rebuild when code changes
- [ ] `new` need implementation