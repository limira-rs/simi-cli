//use std::fs;

use clap;

use crate::config::SimiConfig;
use crate::error::*;
use crate::external_cmds as cmds;

#[derive(Clone)]
pub struct BuildArgs {
    pub release: bool,
    pub nightly: bool,
}

impl BuildArgs {
    pub fn from_clap<'a>(matches: &clap::ArgMatches<'a>) -> Self {
        Self {
            release: matches.is_present("release"),
            nightly: matches.is_present("nightly"),
        }
    }
}

pub fn build(config: &SimiConfig) -> Result<(), Error> {
    cmds::cargo_build(config)?;

    // Create output_path before running wasm-bindgen
    // wasm-bindgen don't like to create new folder
    // *** 0.2.40 support new folder?
    //if config.output_path().canonicalize().is_err() {
    //    fs::create_dir_all(&config.output_path())?;
    //}
    cmds::wasm_bindgen(config)?;

    crate::project::create_simi_app(&config)?;
    Ok(())
}

pub fn run(arg: BuildArgs) -> Result<(), Error> {
    let config = SimiConfig::from_build(arg)?;
    let _ = build(&config)?;
    Ok(())
}
