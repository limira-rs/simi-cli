extern crate clap;
#[macro_use]
extern crate failure;
extern crate simi_cli as simi;

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    if let Err(e) = match clap_app().get_matches().subcommand() {
        ("new", Some(args)) => simi::new::run(simi::new::NewArgs::from_clap(args)),
        ("build", Some(args)) => simi::build::run(simi::build::BuildArgs::from_clap(args)),
        ("serve", Some(args)) => simi::serve::run(simi::serve::ServeArgs::from_clap(args)),
        ("expand", Some(_)) => simi::expand::run(),
        ("test", Some(args)) => simi::test::run(simi::test::TestArgs::from_clap(args)),
        _ => Err(format_err!("Unsupported command")),
    } {
        eprintln!("error: {}", e);
        for cause in e.iter_chain().skip(1) {
            eprintln!("\tcaused by: {}", cause);
        }
        ::std::process::exit(1);
    }
}

fn clap_app<'a, 'b>() -> clap::App<'a, 'b> {
    use clap::{Arg, SubCommand};

    let release = Arg::with_name("release")
        .short("r")
        .long("release")
        .help("Build in release mode");
    let nightly = Arg::with_name("nightly")
        .short("n")
        .long("nightly")
        .help("Build with nightly Rust");

    clap::App::new("simi")
        .version(VERSION)
        .author("Limira")
        .about("Create, build, serve simple wasm app written in Rust")
        .subcommand(
            SubCommand::with_name("new")
                .about("Create new Simi HelloWorld app")
                .arg(
                    Arg::with_name("NAME")
                        .required(true)
                        .help("Name for the new project"),
                ),
        ).subcommand(
            SubCommand::with_name("build")
                .about("Build a Simi app")
                .arg(release.clone())
                .arg(nightly.clone()),
        ).subcommand(SubCommand::with_name("expand").about("Expand to pretty output"))
        .subcommand(
            SubCommand::with_name("test")
                .about("Run your simi app's tests")
                .arg(nightly.clone())
                .arg(
                    Arg::with_name("with_head")
                        .short("w")
                        .long("with_head")
                        .help("Run wasm-bindgen-test with NO_HEADLESS=1"),
                )
                //.arg(
                //     Arg::with_name("nocapture").long("nocapture").help(
                //         "Run test with -- --nocapture to allow println! to output to terminal. It",
                //     ),
                // ),
        ).subcommand(
            SubCommand::with_name("serve")
                .about("Build and serve a Simi app")
                .arg(release)
                .arg(nightly)
                .arg(
                    Arg::with_name("port")
                        .short("p")
                        .long("port")
                        .takes_value(true)
                        .help("A port that simi server bind to"),
                ).arg(
                    Arg::with_name("only")
                        .help("Just serve files at the current directory (Not run cargo build...)"),
                ),
        )
}
