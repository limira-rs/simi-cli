use crate::config::{SimiConfig, SimiStage};
use crate::error::*;
use std::process::Command;

pub fn cargo_build(config: &SimiConfig) -> Result<(), Error> {
    let mut cargo = Command::new("cargo");
    if config.nightly() {
        cargo.arg("+nightly");
    }
    cargo.arg("build").arg("--target").arg(config.target());

    if config.release() {
        cargo.arg("--release");
    }

    println!("{:?}", cargo);
    if cargo.status()?.success() {
        return Ok(());
    };
    Err(format_err!("cargo build failed"))
}

//cargo +nightly rustc -- -Z unstable-options --pretty=expanded
pub fn cargo_expand() -> Result<(), Error> {
    let mut cargo = Command::new("cargo");
    cargo.args(&[
        "+nightly",
        "rustc",
        "--target",
        crate::DEFAULT_TARGET,
        "--",
        "-Z",
        "unstable-options",
        "--pretty=expanded",
    ]);
    println!("{:?}", cargo);
    if cargo.status()?.success() {
        return Ok(());
    };
    Err(format_err!("{:?} failed", cargo))
}

pub fn cargo_test(config: &SimiConfig, driver_path: &str) -> Result<(), Error> {
    let driver_name = driver_path
        .split('/')
        .last()
        .expect("Invalid value for browser driver")
        .to_uppercase();

    let mut cargo = Command::new("cargo");
    cargo.env(driver_name, driver_path);

    if config.with_head() {
        cargo.env("NO_HEADLESS", "1");
    }
    cargo.arg("+nightly");
    cargo.arg("test");
    cargo.arg("--target").arg(config.target());
    //cargo.arg("--").arg("--nocapture");

    println!("{:?}", cargo);
    if cargo.status()?.success() {
        return Ok(());
    };
    Err(format_err!("{:?} failed", cargo))
}

pub fn wasm_bindgen(config: &SimiConfig) -> Result<(), Error> {
    let mut wb = Command::new("wasm-bindgen");
    wb.arg(config.get_wasm_file_path(SimiStage::CargoBuild))
        //.arg("--browser")
        .arg("--target")
        .arg("web")
        .arg("--no-typescript")
        .arg("--out-dir")
        .arg(config.output_path().to_string_lossy().to_string());

    println!("{:?}", wb);
    if wb.status()?.success() {
        return Ok(());
    }
    Err(format_err!("wasm-bindgen failed"))
}

// I just don't want to copy files again if they are not modified
#[cfg(target_os = "linux")]
pub fn copy<P1, P2>(from: &P1, to: &P2) -> Result<(), Error>
where
    P1: AsRef<str>,
    P2: AsRef<str>,
{
    let mut cp = Command::new("cp");
    cp.arg("-u") // Update only
        .arg(from.as_ref())
        .arg(to.as_ref());

    println!("{:?}", cp);
    if cp.status()?.success() {
        return Ok(());
    }
    Err(format_err!(
        "copy failed: {} => {}",
        from.as_ref(),
        to.as_ref()
    ))
}

#[cfg(not(target_os = "linux"))]
pub fn copy<P1, P2>(from: &P1, to: &P2) -> Result<(), Error>
where
    P1: AsRef<str>,
    P2: AsRef<str>,
{
    use std::path::Path;
    println!("Copy: {}", from.as_ref());
    let from = Path::new(from.as_ref());
    let to = Path::new(to.as_ref());
    ::std::fs::copy(from, to)?;
    Ok(())
}
