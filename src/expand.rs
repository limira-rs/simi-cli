use crate::error::*;
use crate::external_cmds as cmds;

pub fn run() -> Result<(), Error> {
    cmds::cargo_expand()?;
    Ok(())
}
