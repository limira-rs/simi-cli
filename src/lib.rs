//#[macro_use]
//extern crate failure_derive;
#[macro_use]
extern crate failure;

extern crate cargo_metadata;
extern crate cargo_toml;
extern crate clap;
extern crate futures;
extern crate hyper;
extern crate mime_guess;
//extern crate notify;
extern crate tokio;
extern crate tokio_fs;
extern crate tokio_io;
extern crate toml;
#[macro_use]
extern crate serde_derive;
extern crate regex;
extern crate rsass;
extern crate serde;
extern crate walkdir;

mod config;
mod external_cmds;
mod project;
mod simple_static_server;

pub mod build;
pub mod error;
pub mod expand;
pub mod new;
pub mod serve;
pub mod test;

const DEFAULT_SERVE_PORT: u16 = 8000;
const DEFAULT_TARGET: &'static str = "wasm32-unknown-unknown";
